from typing import Optional, List
from mhelper import array_helper

import intermake

from csveditor.extensions.operation_control import _view_ops
from csveditor import op_classes, tables
from csveditor.op_classes import EFilter, EMissing, ESortMode, Filter
from csveditor.tables import ColumnAndTableId, TableId


@intermake.command()
def mk_lookup( target: ColumnAndTableId, key: ColumnAndTableId, value: ColumnAndTableId, missing: EMissing = EMissing.ERROR ):
    """
    Adds an operation that looks replaces the text in one column with the text in another column using a specified key column.
    
    Similar to Excel's `LOOKUP` function.
    
    :param target:      Affected column. 
    :param key:         Column to acquire the keys 
    :param value:       Column to acquire the values 
    :param missing:     What to do if a value cannot be found in the other column. 
    """
    if key.table_id.table != value.table_id.table:
        raise ValueError( "Cannot specify two different tables for key ('{}') and value ('{}').".format( key.table_id.table, value.table_id.table ) )
    
    op = op_classes.LookupOperation( target.column_id, key.table_id, key.column_id, value.column_id, missing )
    target.table_id.table.operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_setval( target: ColumnAndTableId, expression: str ):
    """
    Adds an operation that uses a Python expression to modify the table.
    
    :param target:      Target table 
    :param expression:  Expression. 
    """
    op = op_classes.CustomOperation( target.column_id, expression )
    target.table_id.table.operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_pad( target: TableId, default: str = "" ):
    """
    Adds an operation that creates missing values, such that all rows in the table have the same number of cells (as in the headers).
    
    :param target:  Table to modify
    :param default: What to pad with
    """
    op = op_classes.PadOperation( default )
    target.table.operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_replace( target: ColumnAndTableId, find: str, replace: str, regex: bool = False ):
    """
    Adds an operation that finds and replaces text in a specific column.
    
    :param target:    Affected column. 
    :param find:      Text to find.
    :param replace:   Text to replace.
                      Note: If using regular expressions you can use {0} to represent the capture groups.
    :param regex:     Whether to use regular expressions.  
    """
    # Allow {x} to represent group number in replacement
    for i in range( 0, 10 ):
        replace = replace.replace( "{" + str( i ) + "}", "\g<" + str( i ) + ">" )
    
    if regex:
        op = op_classes.ReplaceOperation( target.column_id, find, replace )
    else:
        op = op_classes.ReplaceRxOperation( target.column_id, find, replace )
    
    target.table_id.table.operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_swap( a: ColumnAndTableId, b: ColumnAndTableId ):
    """
    Adds an operation that swaps the values (and headers) of two columns.
    
    :param a:   First affected column.
    :param b:   Second affected column.
    """
    if a.table_id != b.table_id:
        raise ValueError( "a and b must be in the same table." )
    
    op = op_classes.SwapOperation( a.column_id, b.column_id )
    a.table_id.table.operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_drop_row( start: int, end: Optional[int] = None, count: Optional[int] = None ):
    """
    Adds an operation that removes one or more rows from the table.
    
    :param start: Index of first row to drop.
    :param end:   Index one beyond the last row to drop. 
    :param count: Number of rows to drop. Defaults to 1. 
    """
    if end is None:
        if count is None:
            end = start + 1
        else:
            end = start + count
    elif count is not None:
        raise ValueError( "Cannot specify both the `end` and `count` parameters." )
    
    op = op_classes.DropRowOperation( start, end )
    tables.current().operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_drop_col( ids: List[ColumnAndTableId] ):
    """
    Adds an operation that drops one or more columns from the table.
    
    :param ids: One or more column IDs 
    """
    if not ids:
        raise ValueError( "At least one column must be specified." )
    
    for left, right in array_helper.lagged_iterate( ids ):
        if left.table_id != right.table_id:
            raise ValueError( "All columns must be in the same table." )
    
    op = op_classes.DropColOperation( (x.column_id for x in ids) )
    ids[0].table_id.table.operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_filter( col: ColumnAndTableId, filter: EFilter, value: str ):
    """
    Adds an operation that drops rows not passing a filter on a specific column.
    
    :param col:     Column by which to filter
    :param filter:  Filter mode
    :param value:   Value to filter by
    """
    op = op_classes.FilterOperation( col.column_id, value, Filter( filter ) )
    col.table_id.table.operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_sort( col: ColumnAndTableId, mode: ESortMode = ESortMode.NUMERICAL ):
    """
    Adds an operation that sorts the rows by the values in a specific column.
    
    :param col: Column
    :param mode: Sort mode 
    """
    op = op_classes.SortOperation( col.column_id, mode )
    col.table_id.table.operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_unique_u( col: ColumnAndTableId, count: int = 1 ):
    """
    Adds an operation that removes non-unique elements from the table (unix-like).
    
    Like the unix function `uniq`, this assumes the column is sorted and identical elements appear together.
    See `sort` and `unique`.
    
    :param col:     Column to filter. 
    :param count:   Number of unique elements to keep.
    :return: 
    """
    op = op_classes.UniqueUnixOperation( col.column_id, count )
    col.table_id.table.operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_unique( col: ColumnAndTableId, count: int = 1 ):
    """
    Adds an operation that removes non-unique elements from the table.
    
    Unlike `unique_u` this does not assume that the column has been pre-sorted.
    
    :param col:     Column to filter 
    :param count:   Number of unique elements to keep. 
    :return: 
    """
    op = op_classes.UniqueOperation( col.column_id, count )
    col.table_id.table.operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_append( a: ColumnAndTableId, b: ColumnAndTableId, format: str = "{}{}" ):
    """
    Adds an operation that adds the text of one column to the text of another column.
    
    :param a:       First column and target
    :param b:       Second column
    :param format:  Format, in Python format, e.g. `{0}XXX{1}` would be "<first column text>XXX<second column text>". 
    :return: 
    """
    if a.table_id != b.table_id:
        raise ValueError( "a and b must be in the same table." )
    
    op = op_classes.AddOperation( a.column_id, b.column_id, format )
    a.table_id.table.operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_rename( col: ColumnAndTableId, name: str ):
    """
    Adds an operation that changes a column's header text.
    
    :param col:     Column to rename 
    :param name:    New name 
    :return: 
    """
    op = op_classes.RenameHeaderOperation( col.column_id, name )
    col.table_id.table.operations.append( op )
    _view_ops( op )


@intermake.command()
def mk_join( left: ColumnAndTableId, right: ColumnAndTableId, missing: EMissing = EMissing.ERROR, rename: Optional[str] = None ):
    """
    Adds an operation that finds columns in one table matching columns in another table, adding rows from the second table as appropriate.
    
    Similar to an SQL `JOIN`.
    
    :param left:            Column to obtain lookup text from
    :param right:           Column to search for lookup text
    :param missing:         Missing value behaviour. 
    :param rename:          Name of the new columns to be added to the left table, where '*' is the name of the column in the right table.
    """
    if rename is not None and not "*" in rename:
        raise ValueError( "The `rename` parameter should contain the original name placeholder, '*'." )
    
    op = op_classes.JoinOperation( left.column_id, right.table_id, right.column_id, missing, rename )
    left.table_id.table.operations.append( op )
    _view_ops( op )
