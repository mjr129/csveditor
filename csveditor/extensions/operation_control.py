from typing import List, Optional

import intermake

from csveditor import tables
from csveditor.op_classes import Operation



@intermake.command()
def ops():
    """
    Displays the list of operations on the current table.
    """
    _view_ops()


__ICON_ADD = intermake.Theme.STATUS_YES + "<-- NEW" + intermake.Theme.RESET
__ICON_CANCEL = intermake.Theme.STATUS_NO + "<-- REMOVED" + intermake.Theme.RESET
__ICON_SUSPEND = intermake.Theme.STATUS_INTERMEDIATE + "<-- SUSPENDED" + intermake.Theme.RESET
__ICON_RESUME = intermake.Theme.STATUS_INTERMEDIATE + "<-- RESUMED" + intermake.Theme.RESET


def _view_ops( highlight = None, icon = __ICON_ADD ):
    table = tables.current()
    
    r = []
    r.append( str( table ) )
    
    if isinstance( highlight, Operation ):
        highlight = { highlight }
    elif highlight is None:
        highlight = ()
    
    for index, op in enumerate( table.operations ):
        if op in highlight:
            suffix = " " + icon
        else:
            suffix = ""
        
        if op.suspended:
            suffix = " [SUSPENDED]" + suffix
        
        r.append( "{}. {}{}".format( index, op, suffix ) )
    
    if not table.operations:
        r.append( "No operations." )
    
    intermake.pr.pr_information( "\n".join( r ) )


@intermake.command( highlight = True )
def cancel( index: List[int] ):
    """
    Removes an operation. 
    
    :param index:   Index of the operation to drop.
    """
    ops = __get_ops( index )
    _view_ops( highlight = ops, icon = __ICON_CANCEL )
    for i in sorted( index, reverse = True ):
        del tables.current().operations[i]


def __get_ops( index: List[int] ):
    if not index:
        index = list( range( len( tables.current().operations ) ) )
    
    ops = set( tables.current().operations[x] for x in index )
    return ops


@intermake.command()
def suspend( index: Optional[List[int]] = None ):
    """
    Removed an operation temporarily.
    
    :param index:    Index of the operation to drop.
    """
    ops = __get_ops( index )
    _view_ops( highlight = ops, icon = __ICON_SUSPEND )
    for op in ops:
        op.suspended = True


@intermake.command()
def resume( index: Optional[List[int]] = None ):
    """
    Resumes an operation removed via `suspend`.
    
    :param index:    Index of the operation to resume.
                     If this is `-1`, the default, then all suspended operations are resumed.
    """
    ops = __get_ops( index )
    for op in ops:
        op.suspended = False
    _view_ops( highlight = ops, icon = __ICON_RESUME )
