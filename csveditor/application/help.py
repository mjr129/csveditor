import intermake


def add_to( app: intermake.ImApplication ):
    app.help.add( "Specifying columns and tables in |app_name|",
                  """
     To specify a column use the column name:
     
         `artist`
         
     Or the index:
     
         `#1`
         
     You can specify the table name too:
     
         `records:artist`
         
     To specify multiple columns, use comma:
     
         `artist,#2,records:album`
 """ )
