import intermake


class Application( intermake.ImApplication ):
    INSTANCE: "Application" = None
    
    
    def __init__( self, *args, **kwargs ):
        super().__init__( *args, **kwargs )
        
        from . import help
        help.add_to( self )


Application.INSTANCE = Application( name = "CsvEditor" )
