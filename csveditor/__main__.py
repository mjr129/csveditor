# noinspection PyUnresolvedReferences
import csveditor
import intermake

def main():
    """
    Entry point.
    """
    intermake.start()


if __name__ == "__main__":
    main()
